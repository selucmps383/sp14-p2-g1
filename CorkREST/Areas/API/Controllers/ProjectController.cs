﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using CorkREST.Authentication;
using CorkREST.Models;
using CorkREST.DTOs;
using System.Collections.Generic;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByApiKey]
    public class ProjectController : BaseApiController
    {
        private DtoFactory dfac = new DtoFactory();

            public ProjectController(Entities1 context) : base(context)
    {
        
    }

        // GET api/Project
        /// <summary>
        /// Returns a list of projects.
        /// </summary>
        /// <returns></returns>
        public IQueryable<ProjectDTO> GetProjects()
        {
            if (!IsSystemAdmin())
            {
                return CorkUser.Companies.SelectMany(c => c.Projects).AsQueryable<Project>().Select(dfac.AsProjectDTO);
            }

            return _db.Projects.Select(dfac.AsProjectDTO);
        }

        // GET api/Project/5
        /// <summary>
        /// Returns the specified project.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(ProjectDTO))]
        public IHttpActionResult GetProject(int id)
        {
            if (!CanAccessProject(id))
            {
                return Unauthorized();
            }

            ProjectDTO project = _db.Projects
                .Where(p => p.Id == id)
                .Select(dfac.AsProjectDTO)
                .FirstOrDefault();
              
            if (project == null)
            {
                return NotFound();
            }

            return Ok(project);
        }
        
        
        // PUT api/Project/5
        /// <summary>
        /// Updates the specified project.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin,ProjectManager")]
        public IHttpActionResult PutProject(int id, ProjectDTO project)
        {
            if (!CanAccessProject(id)) 
            {
                return Unauthorized();
            }


            if (id != project.Id)
            {
                return BadRequest();
            }

            var projectFromDB = _db.Projects.Find(project.Id);
            if(projectFromDB==null)
            {
                return NotFound();
            }

            if(project.Title != null)
                 projectFromDB.Title = project.Title;
            if(project.CompanyId != 0)
                 projectFromDB.CompanyId = project.CompanyId;
            _db.Entry(projectFromDB).State = EntityState.Modified;

           
            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Project
        /// <summary>
        /// Creates a new project.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin,ProjectManager")]
        [ResponseType(typeof(ProjectDTO))]
        public IHttpActionResult PostProject(ProjectDTO project)
        {
            if(!CanAccessCompany(project.CompanyId))
            {
                return Unauthorized();
            }

            var companyToAssignToo = _db.Companies.Find(project.CompanyId);

            if(companyToAssignToo == null || project.Title == null)
            {
                return BadRequest();
            }

            _db.Projects.Add(dfac.ToProject(project));
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = project.Id }, project);
        }

        // DELETE api/Project/5
        /// <summary>
        /// Deletes a specified project.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin,ProjectManager")]
        [ResponseType(typeof(Project))]
        public IHttpActionResult DeleteProject(int id)
        {
            if (!CanAccessProject(id)) 
            {
                return Unauthorized();
            }

            Project project = _db.Projects.Find(id);
            if (project == null)
            {
                return NotFound();
            }

            _db.Projects.Remove(project);
            _db.SaveChanges();

            return Ok(project);
        }

        private bool ProjectExists(int id)
        {
            return _db.Projects.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using CorkREST.Authentication;
using CorkREST.Models;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByApiKey]
    public class RoleController : BaseApiController
    {
            public RoleController(Entities1 context) : base(context)
    {
        
    }

        // GET api/Role
        /// <summary>
        /// Returns a list of roles.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Role> GetRoles()
        {
            return _db.Roles;
        }

        // GET api/Role/5
        /// <summary>
        /// Returns a specified Role.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Role))]
        public IHttpActionResult GetRole(int id)
        {
            Role role = _db.Roles.Find(id);
            if (role == null)
            {
                return NotFound();
            }

            return Ok(role);
        }

        // PUT api/Role/5
        /// <summary>
        /// Updates a specified Role.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public IHttpActionResult PutRole(int id, Role role)
        {
            return StatusCode(HttpStatusCode.Forbidden);
        }

        // POST api/Role
        /// <summary>
        /// Creates a new Role.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [ResponseType(typeof(Role))]
        public IHttpActionResult PostRole(Role role)
        {
            return StatusCode(HttpStatusCode.Forbidden);
        }

        // DELETE api/Role/5
        /// <summary>
        /// Deletes a specified Role.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Role))]
        public IHttpActionResult DeleteRole(int id)
        {
            return StatusCode(HttpStatusCode.Forbidden);
        }

        private bool RoleExists(int id)
        {
            return _db.Roles.Count(e => e.Id == id) > 0;
        }
    }
}
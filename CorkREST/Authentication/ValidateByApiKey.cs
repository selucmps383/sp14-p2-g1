﻿using CorkREST.Areas.API.Controllers;
using CorkREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace CorkREST.Authentication
{
    public class ValidateByApiKey : AuthorizeAttribute
    {
        private Entities1 db;
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var controller = (BaseApiController)actionContext.ControllerContext.Controller;
            db = controller._db;

            if (actionContext.Request.Headers.Contains(HeaderValues.UserId) &&
               actionContext.Request.Headers.Contains(HeaderValues.Key))
            {
                if (actionContext.Request.Headers.GetValues(HeaderValues.UserId) != null)
                {
                    // get value from header
                    var userIdString = Convert.ToString(actionContext.Request.Headers.GetValues(HeaderValues.UserId).FirstOrDefault());
                    int userId;

                    if (int.TryParse(userIdString, out userId))
                    {
                        var user = db.Users.FirstOrDefault(u => u.Id == userId);

                        if (user != null && actionContext.Request.Headers.GetValues(HeaderValues.Key ) != null)
                        {
                            var apiKey = actionContext.Request.Headers.GetValues(HeaderValues.Key).FirstOrDefault();

                            if (apiKey != user.ApiKey)
                            {
                                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Credentials do not Match or you apiKey is invalid");
                                return;
                            }

                            controller.CorkUser = user;
                            HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
                            return;
                        }
                    }
                    
                }
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Key value " + HeaderValues.UserId + " is not valid");
                return;
            }
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.ExpectationFailed, "Key values " + HeaderValues.UserId + " and/or " + HeaderValues.Key + " were not included in your request.");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.Authentication
{
    public interface ICalculteSignature
    {
        string Signature(string secret, string value);
    }
}
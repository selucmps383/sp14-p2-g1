﻿
using System.Collections.Generic;
namespace CorkREST.Models
{
    public class ReturnUserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> Roles { get;set; }
        public IEnumerable<string> Companies { get; set; }
    
    }
}
﻿using CorkREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Helpers;

namespace CorkREST.DTOs
{
    public class DtoFactory
    {
        public readonly Expression<Func<Defect, DefectDTO>> AsDefectDTO =
        x => new DefectDTO
        {
            Id = x.Id,
            Title = x.Title,
            Description = x.Description,
            ReplicationProcedures = x.ReplicationProcedures,
            Notes = x.Notes,
            UserPainIndex = x.UserPainIndex,
            DateFound = x.DateFound,
            DateFixed = x.DateFixed,
            AssignedToId = x.AssignedToId,
            ProjectId = x.ProjectId,
            ReportedById = x.ReportedById,
            WorkflowStepId = x.WorkflowStepId
        };

        public readonly Func<Defect, DefectDTO> ToDefectDTO =
x => new DefectDTO
{
    Id = x.Id,
    Title = x.Title,
    Description = x.Description,
    ReplicationProcedures = x.ReplicationProcedures,
    Notes = x.Notes,
    UserPainIndex = x.UserPainIndex,
    DateFound = x.DateFound,
    DateFixed = x.DateFixed,
    AssignedToId = x.AssignedToId,
    ProjectId = x.ProjectId,
    ReportedById = x.ReportedById,
    WorkflowStepId = x.WorkflowStepId
};

        public readonly Func<DefectDTO, Defect> ToDefect =
x => new Defect
{
    Id = x.Id,
    Title = x.Title,
    Description = x.Description,
    ReplicationProcedures = x.ReplicationProcedures,
    Notes = x.Notes,
    UserPainIndex = x.UserPainIndex,
    DateFound = x.DateFound,
    DateFixed = x.DateFixed,
    AssignedToId = x.AssignedToId,
    ProjectId = x.ProjectId,
    ReportedById = x.ReportedById,
    WorkflowStepId = x.WorkflowStepId
};

        public readonly Expression<Func<User, ReturnUserDTO>> AsReturnUserDTO =
    x => new ReturnUserDTO
    {
        Id = x.Id,
        FirstName = x.FirstName,
        LastName = x.LastName,
        Email = x.Email,
        Roles = x.Roles.Select(r => r.Name),
        Companies = x.Companies.Select(c=>c.Name)
    };

        public readonly Func<User, ReturnUserDTO> ToReturnUserDTO =
x => new ReturnUserDTO
{
    Id = x.Id,
    FirstName = x.FirstName,
    LastName = x.LastName,
    Email = x.Email,
    Roles = x.Roles.Select(r => r.Name),
    Companies = x.Companies.Select(c => c.Name)
};

        
        public readonly Expression<Func<Company, CompanyDTO>> AsCompanyDTO =
           x => new CompanyDTO
           {
              Id = x.Id,
              Name = x.Name
           };

        public readonly Func<CompanyDTO, Company> ToCompany =
           x => new Company
           {
             Id = x.Id,
             Name = x.Name
           };

        public readonly Expression<Func<Project, ProjectDTO>> AsProjectDTO =
            x => new ProjectDTO
            {
                Id = x.Id,
                Title = x.Title,
                CompanyName = x.Company.Name,
                CompanyId = x.CompanyId,
               
            };

        public readonly Func<ProjectDTO, Project> ToProject =
            x => new Project
            {
                Id = x.Id,
                Title = x.Title,
                CompanyId = x.CompanyId,
               

            };
    }
}
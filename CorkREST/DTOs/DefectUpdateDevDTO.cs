﻿
namespace CorkREST.DTOs
{
    //ROLE: Designer/Developer They
    //can add notes and change the
    //workflow steps only to “In Progress”, “Not Reproducible”, “Ready for
    //Testing”
    public class DefectUpdateDevDTO
    {
        public int DefectId { get; set; }
        public string Notes { get; set; }
        public int WorkflowStepId { get; set; }
    }
}